<?php
return [
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map' => include __DIR__  .'/../template_map.php',
    ],
    'service_manager'   => [
        'aliases' => [
            'dlu_twb_gen_util' => \DluTwBootstrap\GenUtil::class,
            'dlu_twb_form_util' => 'DluTwBootstrap\Form\FormUtil',
            'dlu_twb_nav_view_helper_configurator' => 'DluTwBootstrap\View\Helper\Navigation\PluginConfigurator',
        ],
        'factories' => [
            \DluTwBootstrap\GenUtil::class => \Zend\ServiceManager\Factory\InvokableFactory::class,
            'DluTwBootstrap\Form\FormUtil' => \Zend\ServiceManager\Factory\InvokableFactory::class,
            'DluTwBootstrap\View\Helper\Navigation\PluginConfigurator' => \Zend\ServiceManager\Factory\InvokableFactory::class,
        ]
    ],
    'dlu_tw_bootstrap'  => [
        'sup_ver_zf2'       => '2.0.5',
        'sup_ver_twb'       => '2.1.0',
    ],
    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __DIR__ . '/../public',
            ]
        ],
    ],
];