<?php
namespace DluTwBootstrap\Form;


use Zend\Form\Form;

/**
 * Form Utilities
 * @package DluTwBootstrap
 * @copyright David Lukas (c) - http://www.zfdaily.com
 * @license http://www.zfdaily.com/code/license New BSD License
 * @link http://www.zfdaily.com
 * @link https://bitbucket.org/dlu/dlutwbootstrap
 */
class Search extends Form
{
    /**
     * Constructor
     * @param string|null $defaultFormType
     */
    public function __construct($name = null, $options = array())
    {
    	parent::__construct($name = null, $options = array());
    }
    
}