<?php
namespace DluTwBootstrap\Form\View;

use DluTwBootstrap\GenUtil;
use DluTwBootstrap\Form\FormUtil;

use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\ServiceManager\ServiceManager;

/**
 * HelperConfig
 * Service manager configuration for form view helpers
 * @package DluTwBootstrap
 * @copyright David Lukas (c) - http://www.zfdaily.com
 * @license http://www.zfdaily.com/code/license New BSD License
 * @link http://www.zfdaily.com
 * @link https://bitbucket.org/dlu/dlutwbootstrap
 */
class HelperConfig implements ConfigInterface
{
    /**
     * @var array Pre-aliased view helpers
     */
    protected $invokables = array(
        'form_control_group_twb' => 'DluTwBootstrap\Form\View\Helper\FormControlGroupTwb',
        'form_controls_twb' => 'DluTwBootstrap\Form\View\Helper\FormControlsTwb',
        'form_description_twb' => 'DluTwBootstrap\Form\View\Helper\FormDescriptionTwb',
        'form_element_twb' => 'DluTwBootstrap\Form\View\Helper\FormElementTwb',
        'form_hidden_twb' => 'DluTwBootstrap\Form\View\Helper\FormHiddenTwb',
        'form_hint_twb' => 'DluTwBootstrap\Form\View\Helper\FormHintTwb',
        'formFieldsetTwb' => 'form_fieldset_twb',
        'formRowTwb' => 'form_row_twb',
        'form' => 'form_twb',
    );

    /**
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * @var FormUtil
     */
    protected $formUtil;

    /* ******************** METHODS ******************** */

    /**
     * Constructor
     * @param GenUtil $genUtil
     * @param FormUtil $formUtil
     */
    public function __construct(GenUtil $genUtil, FormUtil $formUtil)
    {
        $this->genUtil = $genUtil;
        $this->formUtil = $formUtil;
    }

    /**
     * Configure the provided service manager instance with the configuration
     * in this class.
     *
     * In addition to using each of the internal properties to configure the
     * service manager, also adds an initializer to inject ServiceManagerAware
     * classes with the service manager.
     *
     * @param  ServiceManager $serviceManager
     * @return void
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        foreach ($this->invokables as $name => $service) {
            $serviceManager->setInvokableClass($name, $service);
        }
        $factories = $this->getFactories();
        foreach ($factories as $name => $factory) {
            $serviceManager->setFactory($name, $factory);
        }
    }

    /**
     * Returns an array of view helper factories
     * @return array
     */
    protected function getFactories()
    {
        $genUtil = $this->genUtil;
        $formUtil = $this->formUtil;
        return array(

            'DluTwBootstrap\Form\View\Helper\FormElementTwb' => InvokableFactory::class,
            'DluTwBootstrap\Form\View\Helper\FormHintTwb' => InvokableFactory::class,
            'DluTwBootstrap\Form\View\Helper\FormHiddenTwb' => InvokableFactory::class,
            'DluTwBootstrap\Form\View\Helper\FormControlGroupTwb' => InvokableFactory::class,
            'DluTwBootstrap\Form\View\Helper\FormControlsTwb' => InvokableFactory::class,
            'DluTwBootstrap\Form\View\Helper\FormDescriptionTwb' => InvokableFactory::class,

            'form_element_unsupported_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormElementUnsupportedTwb($genUtil, $formUtil);
                return $instance;
            },

            'form_actions_twb' => function ($sm) use ($formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormActionsTwb($formUtil);
                return $instance;
            },
            'form_button_twb' => function ($sm) use ($genUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormButtonTwb($genUtil);
                return $instance;
            },
            'form_checkbox_twb' => function ($sm) use ($formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormCheckboxTwb($formUtil);
                return $instance;
            },
            'form_element_errors_twb' => function ($sm) use ($genUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormElementErrorsTwb($genUtil);
                return $instance;
            },
            'form_fieldset_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormFieldsetTwb($genUtil, $formUtil);
                return $instance;
            },
            'form_file_twb' => function ($sm) use ($formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormFileTwb($formUtil);
                return $instance;
            },
            'form_input_twb' => function ($sm) use ($formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormInputTwb($formUtil);
                return $instance;
            },
            'form_label_twb' => function ($sm) use ($genUtil) {
                $sm = $sm->get('ViewHelperManager');
                $formLabelHelper = $sm->get('formLabel');
                $instance = new \DluTwBootstrap\Form\View\Helper\FormLabelTwb($formLabelHelper, $genUtil);
                return $instance;
            },
            'form_multi_checkbox_twb' => function ($sm) use ($genUtil) {
                $sm = $sm->get('ViewHelperManager');
                $formMultiCheckboxHelper = $sm->get('formMultiCheckbox');
                $instance = new \DluTwBootstrap\Form\View\Helper\FormMultiCheckboxTwb(
                    $formMultiCheckboxHelper, $genUtil);
                return $instance;
            },
            'form_password_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormPasswordTwb($genUtil, $formUtil);
                return $instance;
            },
            'form_radio_twb' => function ($sm) use ($genUtil) {
                $sm = $sm->get('ViewHelperManager');
                $formRadioHelper = $sm->get('formRadio');
                $instance = new \DluTwBootstrap\Form\View\Helper\FormRadioTwb(
                    $formRadioHelper, $genUtil);
                return $instance;
            },
            'form_reset_twb' => function ($sm) use ($genUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormResetTwb($genUtil);
                return $instance;
            },
            'form_row_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormRowTwb($genUtil, $formUtil);
                return $instance;
            },
            'form_select_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormSelectTwb($genUtil, $formUtil);
                return $instance;
            },
            'form_submit_twb' => function ($sm) use ($genUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormSubmitTwb($genUtil);
                return $instance;
            },
            'form_text_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormTextTwb($genUtil, $formUtil);
                return $instance;
            },
            'form_textarea_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormTextareaTwb($genUtil, $formUtil);
                return $instance;
            },
            'form_twb' => function ($sm) use ($genUtil, $formUtil) {
                $instance = new \DluTwBootstrap\Form\View\Helper\FormTwb($genUtil, $formUtil);
                return $instance;
            },
        );
    }

    public function toArray()
    {
        return $this->getFactories();
    }


}
